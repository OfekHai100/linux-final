import glob

def main():
	folderList = glob.glob("/var/data/users/*")
	f3 = open("filesContent.txt","w")
	for folder in folderList:
		f1 = open(folder+"/passwd","r")
		f2 = open(folder+"/settings","r")
		f3.write(folder+"/passwd:\n" + f1.read() + "\n" + folder +"/settings:\n" + f2.read() + "\n\n")
		f1.close()
		f2.close()
	f3.close()
	del folderList[:]

if __name__ == "__main__":
    main()
